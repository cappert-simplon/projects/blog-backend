package co.simplon.promo18.blogbackend.controller;

import co.simplon.promo18.blogbackend.entity.Comment;
import co.simplon.promo18.blogbackend.entity.Post;
import co.simplon.promo18.blogbackend.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("/api/comments")
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@Validated
public class CommentsController {

  @Autowired
  private CommentRepository commentRepository;

  @GetMapping()
  public List<Comment> all() {
    return commentRepository.findAllEver();
  }

  @GetMapping("/post/{postId}")
  public List<Comment> findByPostId(@PathVariable int postId) {
    return  commentRepository.findByPost(postId);
  }

  @GetMapping("/{id}")
  public Comment one(@PathVariable int id) {
    return getComment(id);
  }

  // post - Create
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Comment add(@Valid @RequestBody Comment comment) {
    return commentRepository.save(comment);
  }

  // patch (Update)
  @PutMapping("/{id}")
  public Comment update(@PathVariable int id, @Valid @RequestBody Comment comment) {
    Comment toUpdate = this.getComment(id);

    toUpdate.setBody(comment.getBody());
    toUpdate.setAuthor(comment.getAuthor());
    toUpdate.setCreatedAt(comment.getCreatedAt());
    toUpdate.setPostId(comment.getPostId());

    return toUpdate;
  }

  // Delete
  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.CREATED)
  public void delete(@PathVariable int id) {
    commentRepository.deleteById(id);
  }

  private Comment getComment(int id) {
    return commentRepository.findById(id);
  }
}
