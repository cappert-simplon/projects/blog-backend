package co.simplon.promo18.blogbackend.controller;

import co.simplon.promo18.blogbackend.entity.Post;
import co.simplon.promo18.blogbackend.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@RequestMapping("/api/posts")
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@Validated
class PostsController {

  @Autowired private PostRepository postRepository;

  // Read
  @GetMapping()
  public List<Post> all() {
    return postRepository.findAll();
  }

  @GetMapping("/{id}")
  public Post one(@PathVariable int id) {
    return getPost(id);
  }

  // post - Create
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Post add(@Valid @RequestBody Post post) {
    return postRepository.save(post);
  }

  // patch (Update)
  @PutMapping("/{id}")
  public Post update(@PathVariable int id, @Valid @RequestBody Post post) {
    Post toUpdate = this.getPost(id);

    toUpdate.setTitle(post.getTitle());
    toUpdate.setBody(post.getBody());
    toUpdate.setAuthor(post.getAuthor());
    toUpdate.setCreatedAt(post.getCreatedAt());

    return toUpdate;
  }

  // Delete
  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.CREATED)
  public void delete(@PathVariable int id) {
    postRepository.deleteById(id);
    }

  private Post getPost(int id) {
    return postRepository.findById(id);
  }
}
