package co.simplon.promo18.blogbackend.entity;

import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

public class Comment {
  private Integer id;
  @NotNull
  private String body;
  @NotNull
  private String author;
  @NotNull
  private LocalDateTime createdAt;
  @NotNull
  private Integer postId;

  public Comment(Integer id, String body, String author, LocalDateTime createdAt, Integer postId) {
    this.id = id;
    this.body = body;
    this.author = author;
    this.createdAt = createdAt;
    this.postId = postId;
  }

  public Comment(String body, String author, LocalDateTime createdAt, Integer postId) {
    this.body = body;
    this.author = author;
    this.createdAt = createdAt;
    this.postId = postId;
  }

  public Comment() {
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public LocalDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(LocalDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public Integer getPostId() {
    return postId;
  }

  public void setPostId(Integer postId) {
    this.postId = postId;
  }
}
