package co.simplon.promo18.blogbackend.entity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDateTime;

public class Post {
  private Integer id;
  private String title;
  @NotNull
  private String body;
  @NotNull
  private String author;
  @PastOrPresent
  private LocalDateTime createdAt;

  public Post() {
  }

  public Post(String title, String body, String author, LocalDateTime createdAt) {
    this.title = title;
    this.body = body;
    this.author = author;
    this.createdAt = createdAt;
  }

  public Post(Integer id, String title, String body, String author, LocalDateTime createdAt) {
    this.id = id;
    this.title = title;
    this.body = body;
    this.author = author;
    this.createdAt = createdAt;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public LocalDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(LocalDateTime createdAt) {
    this.createdAt = createdAt;
  }
}
