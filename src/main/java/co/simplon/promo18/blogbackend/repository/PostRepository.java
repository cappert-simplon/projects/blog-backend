package co.simplon.promo18.blogbackend.repository;

import co.simplon.promo18.blogbackend.entity.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository public class PostRepository {

  @Autowired
  private DataSource dataSource;

  public List<Post> findAll() {

    List<Post> list = new ArrayList<>();

    // connexion à la base de données
    try (Connection connection = dataSource.getConnection()) {

      PreparedStatement statement = connection.prepareStatement("SELECT * FROM posts");
      ResultSet rs = statement.executeQuery();

      while (rs.next()) {
        Post post = new Post(
            rs.getInt("id"),
            rs.getString("title"),
            rs.getString("body"),
            rs.getString("author"),
            rs.getTimestamp("created_at").toLocalDateTime()
        );
        list.add(post);
      }
      connection.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;

  }


  public Post findById(Integer id) {

    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement statement = connection.prepareStatement("SELECT * FROM posts WHERE id = ?");

      statement.setInt(1, id);

      ResultSet rs = statement.executeQuery();

      if (rs.next()) {
        Post post = new Post(
            rs.getInt("id"),
            rs.getString("title"),
            rs.getString("body"),
            rs.getString("author"),
            rs.getTimestamp("created_at").toLocalDateTime()
        );

        return post;
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return null;

  }


  public Post save(Post post) {
    // return Post with id populated
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("INSERT INTO posts (title, body, author, created_at) VALUES (?,?,?,?)", Statement.RETURN_GENERATED_KEYS);

      stmt.setString(1, post.getTitle());
      stmt.setString(3, post.getBody());
      stmt.setString(3, post.getAuthor());
      stmt.setTimestamp(2, Timestamp.valueOf(post.getCreatedAt()));

      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if(rs.next()) {
        post.setId(rs.getInt(1));
      }

    } catch (SQLException e) {

      e.printStackTrace();
      //  throw new RuntimeException("Database access error");
    }

    return post;

  }

  public void deleteById(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM posts WHERE id=?");
      stmt.setInt(1, id);

      // return (stmt.executeUpdate() == 1);
      stmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    //  throw new RuntimeException("Database access error");
    }
  }
}
