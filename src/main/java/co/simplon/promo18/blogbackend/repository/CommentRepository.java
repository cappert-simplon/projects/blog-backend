package co.simplon.promo18.blogbackend.repository;

import co.simplon.promo18.blogbackend.entity.Comment;
import co.simplon.promo18.blogbackend.entity.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository public class CommentRepository {

  @Autowired private DataSource dataSource;

  // all comments on all posts i.e. for spam moderation
  public List<Comment> findAllEver() {

    List<Comment> list = new ArrayList<>();

    // connexion à la base de données
    try (Connection connection = dataSource.getConnection()) {

      PreparedStatement statement = connection.prepareStatement("SELECT * FROM comments");
      ResultSet rs = statement.executeQuery();

      while (rs.next()) {
        Comment comment = new Comment(rs.getInt("id"), rs.getString("body"), rs.getString("author"),
            rs.getTimestamp("created_at").toLocalDateTime(), rs.getInt("post_id"));
        list.add(comment);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;

  }


  public Comment findById(Integer id) {

    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement statement = connection.prepareStatement("SELECT * FROM comments WHERE id = ?");

      statement.setInt(1, id);

      ResultSet rs = statement.executeQuery();

      if (rs.next()) {
        Comment comment = new Comment(rs.getInt("id"), rs.getString("body"), rs.getString("author"),
            rs.getTimestamp("created_at").toLocalDateTime(), rs.getInt("post_id"));

        return comment;
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return null;

  }

  public List<Comment> findByPost(Integer id) {

    List<Comment> commentList = new ArrayList<>();

    try (Connection connection = dataSource.getConnection()) {

      PreparedStatement statement = connection.prepareStatement("SELECT * FROM comments where post_id = ?");

      statement.setInt(1, id);

      ResultSet rs = statement.executeQuery();

      while (rs.next()) {
        Comment comment = new Comment(rs.getInt("id"), rs.getString("body"), rs.getString("author"),
            rs.getTimestamp("created_at").toLocalDateTime(), rs.getInt("post_id"));
        commentList.add(comment);
      }


    } catch (SQLException e) {
      e.printStackTrace();
    }
    return commentList;
  }


  public Comment save(Comment comment) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement statement = connection.prepareStatement(
          "INSERT INTO comments (body, author, created_at, post_id) VALUES (?,?,?,?)", Statement.RETURN_GENERATED_KEYS);

      statement.setString(1, comment.getBody());
      statement.setString(2, comment.getAuthor());
      statement.setTimestamp(3, Timestamp.valueOf(comment.getCreatedAt()));
      statement.setInt(4, comment.getPostId());

      statement.executeUpdate();

      ResultSet rs = statement.getGeneratedKeys();
      if(rs.next()) {
        comment.setId(rs.getInt(1));
      }


    } catch (SQLException e) {
      throw new RuntimeException(e);
    }

    return comment;
  }

  public void deleteById(Integer id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM comments WHERE id=?");
      stmt.setInt(1, id);

      stmt.executeUpdate();
      // return (stmt.executeUpdate() == 1); to return false as an error


    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }
}
